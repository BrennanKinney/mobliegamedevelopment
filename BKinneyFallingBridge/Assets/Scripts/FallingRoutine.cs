﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingRoutine : MonoBehaviour {

    // Use this for initialization
    [SerializeField]
    Controller C;
    Rigidbody t;
    private const float resetHeight = -5;
    public bool reseting = false;
    public  bool notFalling=true;
    private Vector3 origin;
    void Start() {
        t = GetComponent<Rigidbody>();
        origin = transform.position;
       }
    // Update is called once per frame
    void Update() {
        if (transform.position.y <= resetHeight) {
            reseting = true;
            notFalling = true;
            t.isKinematic = true;
            StartCoroutine(Reset());
        }
    }
	void  OnMouseUpAsButton() {
        if (!C.isCollapsing&&notFalling) {
            fall();
            C.isCollapsing = true;
            StartCoroutine(C.startCollapse(this.gameObject));
        }
    }
    public void fall() {
        notFalling = false;
        t.isKinematic = false;
    }
    public IEnumerator Reset() {
        yield return new WaitForSeconds(1.0f);
        transform.position = new Vector3(transform.position.x, origin.y, transform.position.z);
        reseting = false;
        C.BridgeIsReset();
        
    }
}
