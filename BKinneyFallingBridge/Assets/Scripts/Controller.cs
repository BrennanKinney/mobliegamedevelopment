﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {
    [SerializeField]
    List<GameObject> BridgeParts;
    public bool isCollapsing=false;
    

    public IEnumerator startCollapse(GameObject block) {
        int index = BridgeParts.IndexOf(block);
        int left=index-1;
        int right=index+1;
        while (left >= 0 || right < BridgeParts.Count) {
            yield return new WaitForSeconds(0.5f);
            if (left >= 0) {
                //drop left
                BridgeParts[left].GetComponent<FallingRoutine>().fall();
            }
            if (right < BridgeParts.Count) {
                //drop right
                BridgeParts[right].GetComponent<FallingRoutine>().fall();
            }
            --left;++right;
        }
    }
    public void BridgeIsReset() {
        List<bool> conditions = new List<bool>();
        foreach(GameObject go in BridgeParts) {
            conditions.Add(!go.GetComponent<FallingRoutine>().notFalling || go.GetComponent<FallingRoutine>().reseting);
        }
        isCollapsing = conditions.Contains(true);
    }
}
