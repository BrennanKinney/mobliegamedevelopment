﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    static float seconds = 60;
    Text timer;
    int roundedSeconds = (int)Mathf.Round(seconds);
    float dt;

    // Use this for initialization
    void Start()
    {
         dt= Time.deltaTime;
        timer = GetComponent<Text>();
        
    }

    // Update is called once per frame
    void Update()
    {
         seconds -= dt;
         roundedSeconds = (int)Mathf.Round(seconds);
         timer.text = roundedSeconds.ToString();
        if (roundedSeconds == 0) {
        dt=0;
            UnityEngine.SceneManagement.SceneManager.LoadScene("Loser");
          }
    }
}
