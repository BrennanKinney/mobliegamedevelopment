﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {
    int card;

    public int Id;
    Transform t;
    float dt;
    // Use this for initialization
    void Start() {
        t = this.GetComponent<Transform>();
         card = this.Id;
        dt = Time.deltaTime;
        dt = dt * (float).5;
    }

    // Update is called once per frame
    void OnMouseUpAsButton()    {
      do
        {
            t.Rotate(Vector3.right,dt);
        } while (t.eulerAngles.x < 180);

            GameObject CM =  GameObject.Find("GameObject");
            CheckForMatch checker = CM.GetComponent("CheckForMatch") as CheckForMatch; 
            checker.cards.Add(this.gameObject);
            checker.Compare();
       
        

    }
   public void flipBack() {
        
        do
        {
            t.Rotate(Vector3.left,dt);
        } while (t.eulerAngles.x > 0);
    }

}
