﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BunnyMovement : MonoBehaviour {
    public bool left = true;
    public float speed;
    public float pMax;
    public float pMin;


    // Update is called once per frame
    void Update()
    {
        if (left)
        {
            transform.position += Vector3.left * speed;

            if (transform.position.x <= pMin)
            {
                left = !left;
            }
        }
        if (!left)
        {
            transform.position += Vector3.right * speed;
            if (transform.position.x >= pMax)
            {
                left = !left;
            }
        }
    }
}
