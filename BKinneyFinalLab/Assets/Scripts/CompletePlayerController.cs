﻿using UnityEngine;
using System.Collections;

//Adding this allows us to access members of the UI namespace including Text.
using UnityEngine.UI;

public class CompletePlayerController : MonoBehaviour
{
    public static bool  secretActive=false;
    public float speed;
    private GameObject door;

    private Rigidbody2D rb2d;
    private int score;
    public int completeScore;
    private ParticleSystem ps;
    public static int count=0;
    void Start()
    {
        door = GameObject.Find("Door");
        rb2d = GetComponent<Rigidbody2D>();
        score = 0;
        ps = GetComponent<ParticleSystem>();
        if (secretActive) {
            ps.Play();
        }
        

    }

    void FixedUpdate()
    {
       
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector2 movement = new Vector2(moveHorizontal, moveVertical);
        rb2d.AddForce(movement * speed);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Hazard") || other.gameObject.CompareTag("Enemy"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
            count = 0;
        }
        
        else if (other.gameObject.CompareTag("Door")&&score>= completeScore)
        {
            
            switch (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name) {
                case "Level1":
                    UnityEngine.SceneManagement.SceneManager.LoadScene("Level2");
                    break;
                case "Level2":
                    UnityEngine.SceneManagement.SceneManager.LoadScene("tutorial");
                    break;
                case "tutorial":
                    UnityEngine.SceneManagement.SceneManager.LoadScene("Level4");
                    break;
                case "Level4":
                    UnityEngine.SceneManagement.SceneManager.LoadScene("Level5");
                    break;
                case "Level5":
                    
                    if (count >= 15) {
                        secretActive = true;
                        Debug.Log(secretActive);
                        UnityEngine.SceneManagement.SceneManager.LoadScene("Secert");
                       
                    }
                    else { 
                    UnityEngine.SceneManagement.SceneManager.LoadScene("Winner");
                    }
                    break;
            }

            
        }

        else if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            score = score + 1;
            count = count + 1;
            if (score==completeScore) { 
            door.GetComponent<SpriteRenderer>().color=Color.green;
            }
          

        }
    }

    

}