﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunMovement : MonoBehaviour {
    public bool down = true;
    public float speed;
    public float pMax;
    public float pMin;


    // Update is called once per frame
    void Update() {
        if (down) {

            transform.position += Vector3.down*speed;
           
                if (transform.position.y <= pMin) {
                    down = !down;
                }
            }
            if (!down)
            {
                transform.position += Vector3.up*speed;
                if (transform.position.y >= pMax)
                {
                    down = !down;
                }
            }
        }
    }
