﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCaster : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Input.GetMouseButtonUp(0))
        {
            Vector3 mousePos = Input.mousePosition;
            RaycastHit hit;
            Ray myRay = Camera.main.ScreenPointToRay(mousePos);

            Debug.DrawRay(myRay.origin, myRay.direction, Color.magenta, 1.0f);
            if (Physics.Raycast(myRay, out hit)){
                GameObject thingIHit = hit.transform.gameObject;
                
                thingIHit.GetComponent<Renderer>().material.color = Color.blue;
                thingIHit.GetComponent<Rigidbody>().AddForceAtPosition(Vector3.forward*500,hit.point);
                Debug.Log(myRay.direction*50+" / "+ hit.point);
              
               }
        }
	}
}
