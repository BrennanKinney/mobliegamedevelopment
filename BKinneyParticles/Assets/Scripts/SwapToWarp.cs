﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapToWarp : MonoBehaviour {


    public void Start()
    {
        Application.targetFrameRate = 60;
        StartCoroutine(Swap());
    }

    // Update is called once per frame
    public IEnumerator Swap()
    {

        yield return new WaitForSeconds(6.0f);

        UnityEngine.SceneManagement.SceneManager.LoadScene("Warp");

    }
}
